from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list(request):
    todolist = TodoList.objects.all()
    context = {"todo_list": todolist}
    return render(request, "todos/list.html", context)


def todo_detail(request, id):
    todokey = get_object_or_404(TodoList, id=id)
    context = {
        "todo_key": todokey,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_todo_list = form.save()
            return redirect("todo_list_detail", new_todo_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    edited_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edited_list)
        if form.is_valid():
            new_todo_list = form.save()
            return redirect("todo_list_detail", new_todo_list.id)
    else:
        form = TodoListForm(instance=edited_list)
    context = {"form": form, "edited_list": edited_list}
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    delete_todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
        return redirect("todo_list_detail", id=new_item.list.id)
    # or do i need id=item.list.id
    # use this ^ when you need to access the parent folder/page (TodoList is accessed through the Foreignkey "list") instead of using the id for the object that was created. The current items id is created, just not used.
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/items.html", context)


def edit_todo_item(request, id):
    edited_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edited_item)
        if form.is_valid():
            new_todo_item = form.save()
            return redirect("todo_list_detail", id=new_todo_item.list.id)
    else:
        form = TodoItemForm(instance=edited_item)
    context = {
        "form": form,
        "edited)item": edited_item
        }
    return render(request, "todos/itemedit.html", context)
